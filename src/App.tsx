import React from "react";
import "./App.css";
import { Helmet } from "react-helmet";
import { Router, Route } from "react-router-dom";
import { createBrowserHistory } from "history";
import { hot } from "react-hot-loader";
import { Container, CssBaseline } from "@material-ui/core";

function App() {
  const history = createBrowserHistory();

  return (
    <>
      <Helmet defaultTitle="WB Junior Frontend Interview" />
      <Router history={history}>
        <CssBaseline />
        <Container maxWidth="lg">your code</Container>
      </Router>
    </>
  );
}

export default hot(module)(App);
