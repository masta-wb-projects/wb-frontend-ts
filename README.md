## Задание

Вы получили шаблон сборки для выполнения задания (можно скорректировать под себя).
**Цель задания:** имеется API, за основу для которого взята [TMDb](https://developers.themoviedb.org). Требуется 
создать приложение для поиска фильмов и отображения результата в виде набора карточек (или так, как пожелаете нужным,
на Ваше усмотрение). 
**Достаточно будет отобразить название, год и описание, также можно показать постер.**

**Доп задания:** 
* сделать для каждой сущности "фильм" создать либо отдельный роут, либо модалку/попап с данным
* помимо основных данных имеются: массив жанров, ссылка на IMDb, популярность, рейтинг – при желании можете добавить

#### Запрос:
```
GET https://mastablasta.it/movies/api/v1/search?wb_search_movie=Bourne
```

##### Если не отправить или отправить пустым параметр wb_search_movie, получим ошибку 400 с соответствующим описанием в теле ответа 

#### Ответ:
```
HTTP 200: 
{
    "total_results": 17,
    "results": [
            {
              "genres": [
                {
                  "id": 28,
                  "name": "Action"
                },
                {
                  "id": 18,
                  "name": "Drama"
                },
                {
                  "id": 53,
                  "name": "Thriller"
                }
              ],
              "id": 2502,
              "imdb_url": "https://www.imdb.com/title/tt0372183/",
              "overview": "When a CIA operation to purchase classified Russian documents is blown by a rival agent, who then shows up in the sleepy seaside village where Bourne and Marie have been living. The pair run for their lives and Bourne, who promised retaliation should anyone from his former life attempt contact, is forced to once again take up his life as a trained assassin to survive.",
              "popularity": 26.483,
              "poster_path": "https://image.tmdb.org/t/p/original/7IYGiDrquvX3q7e9PV6Pejs6b2g.jpg",
              "release_date": "2004-07-23",
              "title": "The Bourne Supremacy",
              "vote_average": 7.3
            },
            {...}
    ]
}
```

## Запуск проекта:

### `yarn start`

* Версия node: _12.18.0_
* Версия npm: _6.14.4_
* Если у вас есть [nvm](https://github.com/nvm-sh/nvm#installing-and-updating), то можно 
использовать нужную версию с помощью `nvm use`, предварительно установив нужную версию 
node с помощью nvm: `nvm install 12.18.0`

Если нужен **npm**, пожалуйста, пересоберите проект :)

#### Библиотеки
* Material UI
* lodash

**P.S. Не переживайте, если не знакомы**

Если есть пакеты/библиотеки, которые Вы часто используете, можете их установить, но в целом все, нужное для интервью, уже установлено